#!/usr/bin/env python
from setuptools import setup, find_packages

entry_points = """
[zc.buildout]
phpext = vz.recipe.phpext:PhpExt
hooks = vz.recipe.hooks:Hooks
checkdependancies = vz.recipe.checkdependancies:CheckDependancies
ejabberd = vz.recipe.ejabberd:Recipe

[zc.buildout.uninstall]
hooks = vz.recipe.hooks:uninstall
"""

setup (
    name='vz.recipe',
    description = "Set of helper recipies for zc.buildout",
    version='0.1.2',
    author = "Sebastian Utz",
    author_email = "sutz@vz.net",
    packages = find_packages(),
    include_package_data = True,
    namespace_packages = ['vz', 'vz.recipe'],
    extras_require = dict(),
    install_requires = ['setuptools',
                        'hexagonit.recipe.download',
                        'zc.recipe.egg',
                        'py'
                        ],
    entry_points = entry_points,
    zip_safe = True,
    )
