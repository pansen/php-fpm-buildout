# Common

[Buildout | http://www.buildout.org/] is a software buildsystem for python.
Here it is used to generate sandboxed development environments, so you can start 
totally different setups for your current project.

# Environment

This Buildout sets up a PHP webdevelopment environment with following components

* Supervisor to easyly control the servers
* Nginx
* OpenSSL für Zertifikate und SSL Protokoll
* PHP with apc, xdebug, memcached
* FPM as Part of PHP
* PHPunit
* Composer for PHP
* MySql with phpmyadmin webinterface
* Memcached with memcached admin webinterface
* Buildout tasks to setup symfony2 Project

# Known Issues

## Fix for OSX Mountain Lion
based on http://apple.stackexchange.com/questions/58186/how-to-compile-mod-wsgi-mod-fastcgi-etc-on-mountain-lion-by-fixing-apxserror

    sudo ln -s /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/ /Applications/Xcode.app/Contents/Developer/Toolchains/OSX10.8.xctoolchain

fixes problem with native llvm compiler

## MySql User
in come cases the mysql root user will not be initialized correctly.
If you can not connect to yout database as root try

    parts/mysql/bin/mysqladmin -u root password 'myAdminPassword'

to fix.

# Prerequisites

## OSX General (Lion, Mountain Lion tested)

- Install XCode
- Install "Command Line Tools" from "Preferences > Downloads > Components"
- Install Macports and some dependencies via macports. Those dependencies are intented
  to be independent to any other sandbox. 

    sudo port -v sync
    sudo port -v selfupdate
    sudo port -v install git-core +bash_completion 
    sudo port -v install proj libtool gettext-lint python27 apple-gcc42 cmake py27-pyside libevent
    sudo port -v select python python27
    sudo port -v select --set python python27
    sudo port -v select gcc apple-gcc42

IMPORTANT: close your terminal and open a new one after the last command.


## Buildout General

To build the sandbox its good to set some configuration for buildout. This is to cache
the downloads on a central location as e.g. Maven does this too.

::

    mkdir -p ~/.buildout/downloads
    mkdir -p ~/.buildout/eggs
    mkdir -p ~/.buildout/cache


# Build The Sandbox

## Configuration
look in buildout.config file to check ports and hostnames.
When developing don't forget to update your htaccess file!

## Complete Build   

    python2.7 bootstrap.py -d
    bin/buildout -vvN

## Built Parts

In Some Cases you will neet only to build one or two parts. This can be done with 

    bin/buildout -vvN install <partname>

IMPORTANT: enshure to clean your .installed.cfg file before rebuild

* delete key on top (value in array)
* delete part description (Block)


## Clean All Installed Binaries (aka Reset Everything, take care..)

    rm -rf parts/ .installed.cfg

# Build Symfony Application

    bin/buildout -vvN install symfony2

sets up a symfony sandbox in applications folder. This will be accessible through 
http://sf.<the_domain>
